﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.Animations;
using UnityEngine.SceneManagement;

public class LevelPortal : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player") && other.GetComponent<PlayerLootingController>().hasKey)
        {
            Invoke(nameof(LoadNext), 3);
            GameObject.Find("Canvas").transform.Find("NextLevelScreen").gameObject.SetActive(true);
            
        }
    }
    
    private void LoadNext()
    {
        SceneManager.LoadScene("NextLevel");
    }
}
