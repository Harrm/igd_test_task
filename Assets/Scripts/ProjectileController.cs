using System;
using UnityEngine;
using Object = System.Object;

public class ProjectileController : MonoBehaviour
{
    public float speed = 5.0f;

    private void Start()
    {
        Destroy(gameObject, 15);
    }

    private void FixedUpdate()
    {
        transform.Translate(speed * Time.fixedDeltaTime * -transform.right);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        Destroy(gameObject);
    }
}