﻿
using UnityEngine;
using Random = UnityEngine.Random;

public class RangedEnemyAttackController : MonoBehaviour
{
    public Animator animator;
    public GameObject projectile;
    public float shotTimeMin = 3.0f;    
    public float shotTimeMax = 5.0f;

    public void Shoot()
    {
        if (projectile)
        {
            animator.SetTrigger("Attack");
            Instantiate(projectile, transform.position, Quaternion.LookRotation(transform.right, transform.up));
        }
    }
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        timeSinceLastShot += Time.fixedDeltaTime;

        var probability = Time.fixedDeltaTime/(shotTimeMax - shotTimeMin);
        if ((timeSinceLastShot > shotTimeMin && Random.Range(0.0f, 1.0f) < probability) || timeSinceLastShot > shotTimeMax)
        {
            Shoot();
            timeSinceLastShot = 0;
        }
    }

    private float timeSinceLastShot = 0.0f;
}
