using System;
using UnityEngine;

public class MeleeEnemyMovementController : MonoBehaviour
{

    public Vector3 startPosition;
    public float wanderRadius = 10.0f;
    public float speed = 5.0f;
    
    void Start()
    {
        startPosition = gameObject.transform.position;
        movementController = GetComponent<BasicMovementController>();
        direction = Math.Abs(speed);
    }

    void FixedUpdate()
    {
        var pos = gameObject.transform.position;
        float dx = pos.x - startPosition.x;
        if (Math.Abs(dx) >= wanderRadius)
        {
            direction *= -1;
        }
        movementController.SetHorizontalSpeed(direction);
    }
    
    private BasicMovementController movementController;
    private float direction;
}