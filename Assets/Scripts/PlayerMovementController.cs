﻿using System;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.SceneManagement;

public class PlayerMovementController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        movementController = GetComponent<BasicMovementController>();
    }

    void FixedUpdate()
    {
        if (health < 1) return;
        var jump = Input.GetKeyDown("space");
        var haxis = Input.GetAxis("Horizontal");
        if (jump)
        {
            movementController.MaybeDoubleJump(jumpSpeed);
        }
        movementController.SetHorizontalSpeed(haxis * runSpeed);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("DeadlyTouch"))
        {
            OnDamaged();
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("DeadlyTouch"))
        {
            OnDamaged();
        }
    }

    private void OnDamaged()
    {
        health -= 1;
        if (health < 1)
        {
            movementController.animator.SetTrigger("Dead");
            Invoke(nameof(GameOver), 1);
            movementController.SetHorizontalSpeed(0);
            GameObject.Find("Canvas").transform.Find("GameOverScreen").gameObject.SetActive(true);
        }
        
    }

    private void GameOver()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    [SerializeField] private float runSpeed = 5.0f;
    [SerializeField] private float jumpSpeed = 10f;
    [SerializeField]private int health = 1;
    private BasicMovementController movementController;
}