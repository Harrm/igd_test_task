using System;
using UnityEngine;

public class BasicMovementController : MonoBehaviour
{
    public Animator animator;

    public void Jump(float force)
    {
        if (grounded)
        {
            rbody.AddForce(new Vector2(0, force + startJumpForce), ForceMode2D.Impulse);
        }
    }

    public void MaybeDoubleJump(float force)
    {
        if (grounded || doubleJumpAllowed)
        {
            rbody.velocity = Vector2.zero;
            rbody.AddForce(new Vector2(0, force + startJumpForce) * (grounded ? 1.0f : 0.8f), ForceMode2D.Impulse);
            if (!grounded)
            {
                doubleJumpAllowed = false;
            }
        }
    }
    
    public void SetHorizontalSpeed(float speed)
    {
        horizontalSpeed = speed;
    }

    // Start is called before the first frame update
    void Start()
    {
        rbody = GetComponent<Rigidbody2D>();
    }

    void FixedUpdate()
    {
        var hmovement = horizontalSpeed * Time.fixedDeltaTime;

        var hash = Animator.StringToHash("Speed");
        animator.SetFloat(hash, Math.Abs(hmovement));
        transform.Translate(new Vector3(hmovement, 0));
        if (Math.Abs(hmovement) > 0.001f)
        {
            var prevScale = transform.localScale;
            transform.localScale = new Vector3( Math.Abs(prevScale.x * hmovement) / hmovement, prevScale.y, prevScale.z);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Ground"))
        {
            grounded = true;
            doubleJumpAllowed = true;
            animator.SetTrigger("Grounded");
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Ground"))
        {
            grounded = false;
            animator.SetTrigger("InAir");
        }
    }

    [SerializeField] private float horizontalSpeed = 5.0f;
    [SerializeField] private float startJumpForce = 10.0f;
    [SerializeField] private bool grounded = true;
    [SerializeField] private bool doubleJumpAllowed = true;
    private Rigidbody2D rbody;
}