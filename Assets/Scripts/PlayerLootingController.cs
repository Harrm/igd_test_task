﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerLootingController : MonoBehaviour
{
    public int coins = 0;
    public bool hasKey = false;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Coin"))
        {
            coins++;
            Destroy(other.gameObject);
            GameObject.Find("CoinsCountText").GetComponent<Text>().text = "Coins: " + coins;
        }

        if (other.gameObject.CompareTag("Key"))
        {
            hasKey = true;
            Destroy(other.gameObject);
            GameObject.Find("Canvas").transform.Find("KeyObtainedText").gameObject.SetActive(true);
        }
    }
}
